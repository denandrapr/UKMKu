package com.example.ukmku.api

import com.example.ukmku.models.*
import com.example.ukmku.models.minat_bakat.GetMinatBakatResults
import com.example.ukmku.models.minat_bakat.SendMinatBakatResult
import com.example.ukmku.models.minat_bakat.SoalMinatBakat
import com.example.ukmku.models.send_personality.PersonalitySendResult
import com.example.ukmku.models.users.UpdateUserResponse
import com.example.ukmku.models.users.UserResponse
import retrofit2.Call
import retrofit2.http.*

interface DataServices {

    /*
    Request to handle user login
     */
    @POST("users/login")
    @FormUrlEncoded
    fun usersLogin(
            @Field("email") email: String,
            @Field("password") password: String
    ):Call<UserResponse>

    /*
    Request to handle user register
     */
    @POST("users/register")
    @FormUrlEncoded
    fun usersRegister(
        @Field("email") email:String,
        @Field("password") password:String,
        @Field("name") name:String
    ):Call<UserResponse>

    /*
    Request to get
     */
    @GET("question")
    fun getQuestion() : Call<List<GetQuestion>>

    /*
    Request to get personality test
     */
    @GET("ptest")
    fun getpTest() : Call<List<GetTest>>

    /*
    Request to send personality quest
     */
    @POST("presult/result")
    @FormUrlEncoded
    fun sendTestResult(
        @Field("email") email:String,
        @Field("results") result: ArrayList<String>
    ):Call<PersonalitySendResult>

    /*
    Request to get personality result
     */
    @GET("personality/{personality}")
    fun getMyPersonality(
            @Path("personality") personality: String
    ): Call<PersonalitySendResult>

    /*
    Request to get interest test
     */
    @GET("itest")
    fun getMinatBakatQuestion(): Call<List<SoalMinatBakat>>

    /*
    Request to send interest quest
     */
    @POST("iresult/result")
    @FormUrlEncoded
    fun sendMinatBakatResult(
            @Field("email") email:String,
            @Field("results") result: ArrayList<String>
    ):Call<GetMinatBakatResults>

    /*
    Request to get interest result
     */
    @POST("/iresult/report")
    @FormUrlEncoded
    fun getMinatBakatTop3(
            @Field("email") personality: String
    ): Call<GetMinatBakatResults>

    /*
    Request to send updated data user
     */
    @FormUrlEncoded
    @PUT("/users/{id}/update")
    fun sendUpdatedData(
        @Path("id") _id: String,
        @Field("name") name: String,
        @Field("email") email: String,
        @Field("birth") birth: String ,
        @Field("campus") campus: String,
        @Field("study_program") study_program: String,
        @Field("description") description: String
    ): Call<UpdateUserResponse>
}