package com.example.ukmku.view.kenali_diri.adapter

import android.util.Log
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.example.ukmku.models.minat_bakat.SoalMinatBakat
import com.example.ukmku.view.temukan_bakat.soal.SoalMinatBakatFragment

class SoalMinatBakatAdapter(fragmentManager: FragmentManager,
                            private val soal: List<SoalMinatBakat>) : FragmentStatePagerAdapter(fragmentManager){

    override fun getItem(position: Int): Fragment {
        return SoalMinatBakatFragment.newInstance(soal[position], soal.size, position)
    }

    override fun getCount(): Int {
        return soal.size
    }
}