package com.example.ukmku.view.kenali_diri.main

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.ukmku.R
import kotlinx.android.synthetic.main.activity_kenali_diri_main.*


class KenaliDiriMainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_kenali_diri_main)

        mulai.setOnClickListener {
            val bottomSheetFragment = BottomSheetFragment()
            bottomSheetFragment.show(supportFragmentManager, bottomSheetFragment.getTag())
        }
    }
}
