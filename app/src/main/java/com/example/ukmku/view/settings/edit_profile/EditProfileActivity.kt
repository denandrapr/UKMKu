@file:Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")

package com.example.ukmku.view.settings.edit_profile

import android.annotation.SuppressLint
import android.app.DatePickerDialog
import android.app.DatePickerDialog.OnDateSetListener
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.ukmku.R
import com.example.ukmku.models.users.UpdateUserResponse
import com.example.ukmku.utils.SharedPreferenceManager
import com.example.ukmku.view.profile_edit.IProfileEditView
import com.example.ukmku.view.profile_edit.ProfileEditPresenter
import kotlinx.android.synthetic.main.activity_edit_profile.*
import java.text.SimpleDateFormat
import java.util.*


class EditProfileActivity : AppCompatActivity(), IProfileEditView {

    lateinit var campus:String
    lateinit var name:String
    lateinit var email:String
    lateinit var dateBirth:String
    lateinit var studyProgram: String
    lateinit var desc: String
    lateinit var id: String

    private var mYear = 0
    private  var mMonth:Int = 0
    private  var mDay:Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_profile)

        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        initText()

        dateButton()
        prodiButton()
        saveButton()
    }

    private fun saveButton() {
        btn_save.setOnClickListener {
            val newName = txtNama.text.toString()
            val newEmail = txtEmail.text.toString()
            val newDateBirth = txtBirth.text.toString()
            val newCampus = txtCampus.text.toString()
            val newStudyProgram = txtProgramStudy.text.toString()
            val newDesc = txtDesc.text.toString()

            if (newName.isEmpty() &&
                    newEmail.isEmpty() &&
                    newDateBirth.isEmpty() &&
                    newCampus.isEmpty() &&
                    newStudyProgram.isEmpty() &&
                    newDesc.isEmpty()){
                Toast.makeText(applicationContext, "Tidak boleh ada data kosong!", Toast.LENGTH_SHORT).show()
            }else{
                Log.d("Response send", "$newName $newEmail $newDateBirth $newCampus $newStudyProgram $newDesc")
                ProfileEditPresenter(this).sendUpdatedData(id, newName, newEmail, newDateBirth, newCampus, newStudyProgram, newDesc)
            }
        }
    }

    private fun prodiButton() {

    }

    @SuppressLint("SimpleDateFormat", "DefaultLocale")
    private fun dateButton() {
        txtBirth.setOnClickListener {
            // Get Current Date
            val c: Calendar = Calendar.getInstance()
            mYear = c.get(Calendar.YEAR)
            mMonth = c.get(Calendar.MONTH)
            mDay = c.get(Calendar.DAY_OF_MONTH)

            val datePickerDialog = DatePickerDialog(this,
                    OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                        val dateRaw = (dayOfMonth.toString() + "-" + (monthOfYear + 1) + "-" + year)
                        val dateRawParsed = SimpleDateFormat("dd-MM-yyyy").parse(dateRaw)
                        val dateFormat = SimpleDateFormat("dd MMMM yyyy")
                        val dateFormated:String = dateFormat.format(dateRawParsed)
                        txtBirth.text = dateFormated.decapitalize()
                    }, mYear, mMonth, mDay)
            datePickerDialog.show()
        }
    }

    private fun initText() {
        val sharedRef = SharedPreferenceManager.getInstance(this)

        campus = sharedRef.user.campus.toString()
        name = sharedRef.user.name.toString()
        email = sharedRef.user.email.toString()
        dateBirth = sharedRef.user.birth.toString()
        studyProgram = sharedRef.user.study_program.toString()
        desc = sharedRef.user.description.toString()
        id = sharedRef.user._id.toString()

        txtNama.setText(name)
        txtEmail.setText(email)

        if (campus != "none"){
            txtCampus.setText(campus)
        }
        if (dateBirth != "none"){
            txtBirth.setText(dateBirth)
        }
        if (studyProgram != "none"){
            txtProgramStudy.setText(studyProgram)
        }
        if (desc != "none"){
            txtDesc.setText(desc)
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun onDataUpdateComplete(responseLogin: UpdateUserResponse) {
        if (responseLogin.Message.equals("Data Updated!", ignoreCase = true)){
            Toast.makeText(applicationContext, responseLogin.Message, Toast.LENGTH_SHORT).show()
        }

    }

    override fun onDataUpdateUnSuccess(throwable: Throwable) {
        Toast.makeText(applicationContext, ""+throwable, Toast.LENGTH_SHORT).show()
    }
}
