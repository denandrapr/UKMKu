package com.example.ukmku.view.home

import android.content.ClipDescription
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.afollestad.materialdialogs.MaterialDialog
import com.bumptech.glide.Glide
import com.example.ukmku.R
import com.example.ukmku.utils.SharedPreferenceManager
import com.example.ukmku.view.kenali_diri.hasil.KenaliDiriHasilActivity
import com.example.ukmku.view.kenali_diri.main.KenaliDiriMainActivity
import com.example.ukmku.view.temukan_bakat.hasil.MinatBakatHasilActivity
import com.example.ukmku.view.temukan_bakat.main.TemukanBakatmuActivity
import com.example.ukmku.view.temukan_ukm.TemukanUKMActivity
import kotlinx.android.synthetic.main.fragment_home.view.*
import kotlinx.android.synthetic.main.item_characteristic_personality.*

class HomeFragment : Fragment(){

    lateinit var personality: String
    lateinit var interest: String
    lateinit var description: String
    lateinit var study_program: String
    lateinit var campus: String
    lateinit var birth: String

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val root = inflater.inflate(R.layout.fragment_home, container, false)

        Glide.with(this)
                .load(R.drawable.ic_banner_new)
                .into(root.banner)

        val sharedRef = SharedPreferenceManager.getInstance(root.context)
        val sharedPreferences = context?.getSharedPreferences("ukmku_pref", Context.MODE_PRIVATE)

        personality = sharedRef.user.personality.toString()
        interest = sharedPreferences?.getString("interest", null).toString()
        description = sharedRef.user.description.toString()
        study_program = sharedRef.user.description.toString()
        campus = sharedRef.user.campus.toString()
        birth = sharedRef.user.birth.toString()


        Log.d("Response", ""+personality)
        Log.d("Response", ""+interest)

        root.cardKepribadian.setOnClickListener {
            if (description.equals("none") &&
                    study_program.equals("none") &&
                    campus.equals("none") &&
                    birth.equals("none")){
                MaterialDialog(root.context).show {
                    title(text = "Warning")
                    message(text = "Tidak bisa mengikuti test, lengkapi profil terlebih dahulu!")
                    positiveButton (R.string.positive_text){
//                        Toast.makeText(root.context, "positive", Toast.LENGTH_SHORT).show()
                    }
                }
            }else{
                if (personality.equals("none")){
                    val i = Intent(root.context, KenaliDiriMainActivity::class.java)
                    root.context?.startActivity(i)
                }else{
                    val i = Intent(root.context, KenaliDiriHasilActivity::class.java)
                    root.context?.startActivity(i)
                }
            }
        }

        root.cardMinatBakat.setOnClickListener {
            if (interest.equals("none")){
                val ii = Intent(root.context, TemukanBakatmuActivity::class.java)
                root.context?.startActivity(ii)
            }else{
                val ii = Intent(root.context, MinatBakatHasilActivity::class.java)
                root.context?.startActivity(ii)
            }
        }

        root.cardTemukanUKM.setOnClickListener {
            val i = Intent(root.context, TemukanUKMActivity::class.java)
            root.context?.startActivity(i
            )
        }

        return root
    }

}
