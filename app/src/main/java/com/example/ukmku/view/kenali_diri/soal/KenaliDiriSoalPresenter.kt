package com.example.ukmku.view.kenali_diri.soal

import android.content.Context
import android.util.Log
import com.example.ukmku.api.DataServices
import com.example.ukmku.api.ServiceBuilder
import com.example.ukmku.models.GetTest
import com.example.ukmku.models.send_personality.PersonalitySendResult
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class KenaliDiriSoalPresenter(context: Context) {

    val soalView = context as IKenaliDiriSoalView

    fun getKenaliDiriSoal(){
        val dataServices = ServiceBuilder.buildService(DataServices::class.java)
        val requestCall = dataServices.getpTest()

        requestCall.enqueue(object : Callback<List<GetTest>> {
            override fun onResponse(call: Call<List<GetTest>>, response: Response<List<GetTest>>) {
                val body = response.body()!!
                soalView.onDataGetSoalComplete(body)
            }

            override fun onFailure(call: Call<List<GetTest>>, t: Throwable) {
                soalView.onDataGetSoalError(t)
            }
        })
    }

    fun sendKenaliDiriSoal(email: String, hasil: ArrayList<String>) {
        val dataServices = ServiceBuilder.buildService(DataServices::class.java)
        val requestCall = dataServices.sendTestResult(email, hasil)

        requestCall.enqueue(object : Callback<PersonalitySendResult>{

            override fun onResponse(call: Call<PersonalitySendResult>, response: Response<PersonalitySendResult>) {
                val body = response.body()!!
                Log.d("Response", ""+body)
                soalView.onDataSendSoalComplete(body)
            }

            override fun onFailure(call: Call<PersonalitySendResult>, t: Throwable) {
                soalView.onDataSendSoalError(t)
            }
        })
    }
}