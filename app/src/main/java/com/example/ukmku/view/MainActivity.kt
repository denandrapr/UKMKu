package com.example.ukmku.view;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.Handler
import android.widget.FrameLayout
import android.widget.Toast
import androidx.fragment.app.Fragment

import com.example.ukmku.R;
import com.example.ukmku.view.home.HomeFragment
import com.example.ukmku.view.menu.MenuFragment
import com.example.ukmku.view.profile.ProfileFragment
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(){

    private var content: FrameLayout? = null
    private var doubleBackToExitPressedOnce = false

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when(item.itemId){
            R.id.menu ->{
                val fragment = MenuFragment()
                addFragment(fragment)
                return@OnNavigationItemSelectedListener true
            }
            R.id.home ->{
                val fragment = HomeFragment()
                addFragment(fragment)
                return@OnNavigationItemSelectedListener true
            }
            R.id.profile ->{
                val fragment = ProfileFragment()
                addFragment(fragment)
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }

    private fun addFragment(fragment: Fragment) {
        supportFragmentManager
                .beginTransaction()
                .replace(R.id.content, fragment, fragment.javaClass.getSimpleName())
                .commit()
    }

    override fun onCreate(savedInstanceState: Bundle?){
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        nav_bottom.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
        nav_bottom.setSelectedItemId(R.id.home)

        val fragment = HomeFragment()
        addFragment(fragment)
    }

    override fun onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed()
            return
        }

        this.doubleBackToExitPressedOnce = true
        Toast.makeText(this, "tekan sekali lagi untuk keluar", Toast.LENGTH_SHORT).show()

        Handler().postDelayed(Runnable { doubleBackToExitPressedOnce = false }, 2000)
    }
}