package com.example.ukmku.view.register

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.example.ukmku.R
import com.example.ukmku.models.users.UserResponse
import com.example.ukmku.utils.SharedPreferenceManager
import com.example.ukmku.view.MainActivity
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_register_member.*

class RegisterMemberActivity : AppCompatActivity(), IRegisterView {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register_member)

        actionBarInit()

        btn_masuk.setOnClickListener {
            registerAction()
        }
    }

    private fun actionBarInit(){
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
    }

    private fun registerAction() {
        val name:String = txtNama.text.toString()
        val email:String = txtEmail.text.toString()
        val password:String = textPassword.text.toString()
        val re_password:String = textRePassword.text.toString()

        if (name.isEmpty() && email.isEmpty() && password.isNullOrEmpty() && re_password.isNullOrEmpty()){
            Toast.makeText(this, "Tidak boleh kosong",  Toast.LENGTH_SHORT).show()
        }else{
            if (password.equals(re_password)){
                if (password.length >= 6){
                    showSpin()
                    RegisterPresenter(this).sendRegisterData(email, name, password)
                }else{
                    hideSpin()
                    Toast.makeText(applicationContext, "Password harus lebih dari 6 huruf", Toast.LENGTH_SHORT).show()
                }
            }else{
                hideSpin()
                Toast.makeText(applicationContext, "Password tidak sama", Toast.LENGTH_SHORT).show()
            }
        }

    }

    override fun onDataRegisterComplete(responseRegist: UserResponse) {
        hideSpin()
        Toast.makeText(applicationContext, "Sukses login", Toast.LENGTH_SHORT).show()
        sharedPreferenceSetting(responseRegist)
    }

    override fun onDataLoginErrorFromApi(throwable: Throwable) {
        Toast.makeText(applicationContext, "Error "+throwable.localizedMessage, Toast.LENGTH_SHORT).show()
    }

    private fun sharedPreferenceSetting(responseRegist: UserResponse) {
        SharedPreferenceManager.getInstance(applicationContext).saveUser(responseRegist)
        val intent = Intent(applicationContext, MainActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(intent)
    }

    private fun showSpin(){
        spin_kit.visibility = View.VISIBLE
        scroll.visibility = View.GONE
        btn_masuk.visibility = View.GONE
    }

    private fun hideSpin(){
        spin_kit.visibility = View.GONE
        scroll.visibility = View.VISIBLE
        btn_masuk.visibility = View.VISIBLE
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}
