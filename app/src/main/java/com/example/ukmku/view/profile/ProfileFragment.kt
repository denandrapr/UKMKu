package com.example.ukmku.view.profile

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.example.ukmku.R
import com.example.ukmku.utils.SharedPreferenceManager
import com.example.ukmku.view.settings.SettingsActivity
import kotlinx.android.synthetic.main.fragment_profile.view.*

class ProfileFragment : Fragment(){
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val root = inflater.inflate(R.layout.fragment_profile, container, false)

        Glide.with(this)
                .load(R.drawable.ic_banner_profile)
                .into(root.imageView2)

        root.settings.setOnClickListener {
            val intent = Intent(root.context, SettingsActivity::class.java)
            startActivity(intent)
        }

        val sharedRef = SharedPreferenceManager.getInstance(root.context)

        val name:String = sharedRef.user.name.toString()
        val desc: String = sharedRef.user.description.toString()
        val personality: String = sharedRef.user.personality.toString()

        root.txtDescription.text = desc
        root.textViewName.text = name
        root.txtPersonality.text = personality

        return root
    }
}