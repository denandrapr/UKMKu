package com.example.ukmku.view.kenali_diri.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.example.ukmku.models.GetTest
import com.example.ukmku.view.kenali_diri.soal.SoalFragment

class SoalKenaliDiriAdapter(fragmentManager: FragmentManager,
                            private val soal: List<GetTest>) : FragmentStatePagerAdapter(fragmentManager){

    override fun getItem(position: Int): Fragment {

        return SoalFragment.newInstance(soal[position], soal.size, position)
    }

    override fun getCount(): Int {
        return soal.size
    }
}