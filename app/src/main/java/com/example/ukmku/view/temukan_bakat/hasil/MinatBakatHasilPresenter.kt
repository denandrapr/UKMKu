package com.example.ukmku.view.temukan_bakat.hasil

import android.content.Context
import com.example.ukmku.api.DataServices
import com.example.ukmku.api.ServiceBuilder
import com.example.ukmku.models.minat_bakat.GetMinatBakatResults
import com.example.ukmku.models.send_personality.PersonalitySendResult
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MinatBakatHasilPresenter(context: Context) {

    val hasilView = context as IMinatBakatHasil

    fun getHasil(email: String){

        val dataServices = ServiceBuilder.buildService(DataServices::class.java)
        val requestCall = dataServices.getMinatBakatTop3(email)

        requestCall.enqueue(object : Callback<GetMinatBakatResults> {
            override fun onResponse(call: Call<GetMinatBakatResults>, response: Response<GetMinatBakatResults>) {
                val body = response.body()!!

                hasilView.onDataGetDataMinatComplete(body)
            }

            override fun onFailure(call: Call<GetMinatBakatResults>, t: Throwable) {
                hasilView.onDataGetKepribadianError(t)
            }
        })
    }
}