package com.example.ukmku.view.kenali_diri.hasil

import android.content.Context
import com.example.ukmku.api.DataServices
import com.example.ukmku.api.ServiceBuilder
import com.example.ukmku.models.send_personality.PersonalitySendResult
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class KenaliDiriHasilPresenter(context: Context) {

    val hasilView = context as IKenaliDiriHasilView

    fun getHasil(personality: String){

        val dataServices = ServiceBuilder.buildService(DataServices::class.java)
        val requestCall = dataServices.getMyPersonality(personality)

        requestCall.enqueue(object : Callback<PersonalitySendResult> {
            override fun onResponse(call: Call<PersonalitySendResult>, response: Response<PersonalitySendResult>) {
                val body = response.body()!!

                hasilView.onDataGetKepribadianComplete(body)
            }

            override fun onFailure(call: Call<PersonalitySendResult>, t: Throwable) {
                hasilView.onDataGetKepribadianError(t)
            }
        })
    }
}