package com.example.ukmku.view.temukan_bakat.hasil

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.example.ukmku.R
import com.example.ukmku.models.minat_bakat.GetMinatBakatResults
import com.example.ukmku.utils.SharedPreferenceManager
import com.example.ukmku.view.MainActivity
import com.example.ukmku.view.temukan_bakat.adapter.TopInterestAdapter
import kotlinx.android.synthetic.main.activity_minat_bakat_hasil.*


class MinatBakatHasilActivity : AppCompatActivity(), IMinatBakatHasil {

    lateinit var name: String
    lateinit var email: String

    private lateinit var linearLayoutTopInterest: LinearLayoutManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_minat_bakat_hasil)

        Glide.with(this)
                .load(R.drawable.ic_banner_minat_bakat_hasil)
                .into(banner)

        val sharedRef = SharedPreferenceManager.getInstance(this)

        name = sharedRef.user.name.toString()
        email = sharedRef.user.email.toString()

        txtNamaAkun.text = name

        MinatBakatHasilPresenter(this).getHasil(email)

        selesai.setOnClickListener {
            val i = Intent(this, MainActivity::class.java)
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            startActivity(i)
        }

    }

    override fun onDataGetDataMinatComplete(body: GetMinatBakatResults) {
        linearLayoutTopInterest = LinearLayoutManager(this)

        RecyclerTopInterest.layoutManager = linearLayoutTopInterest
        RecyclerTopInterest.adapter = TopInterestAdapter(body, applicationContext)

        Log.d("Response", ""+body)
    }

    override fun onDataGetKepribadianError(throwable: Throwable) {
        Log.d("Response", ""+throwable)
    }

}
