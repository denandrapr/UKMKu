package com.example.ukmku.view.login

import com.example.ukmku.models.users.UserResponse

interface ILoginView {
    fun onDataLoginComplete(responseLogin : UserResponse)
    fun onDataLoginUnSuccess()
    fun onDataLoginErrorFromApi(throwable : Throwable)
}