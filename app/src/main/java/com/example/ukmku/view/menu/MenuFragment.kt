package com.example.ukmku.view.menu

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.example.ukmku.R
import kotlinx.android.synthetic.main.fragment_home.view.*

class MenuFragment : Fragment(){
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val root = inflater.inflate(R.layout.fragment_menu, container, false)
        Glide.with(this)
                .load(R.drawable.ic_banner_ukm)
                .into(root.banner)
        return root
    }
}