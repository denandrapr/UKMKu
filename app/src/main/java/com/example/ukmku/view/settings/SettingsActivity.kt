package com.example.ukmku.view.settings

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.afollestad.materialdialogs.MaterialDialog
import com.example.ukmku.R
import com.example.ukmku.utils.SharedPreferenceManager
import com.example.ukmku.view.login.LoginActivity
import com.example.ukmku.view.settings.edit_profile.EditProfileActivity
import kotlinx.android.synthetic.main.activity_settings.*

class SettingsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settings)

        /*
        Handling logout button
         */
        logout.setOnClickListener {
            MaterialDialog(this).show {
                title(text = "Warning")
                message(text = "Apa anda yakin ingin logout?")
                positiveButton (R.string.positive_setting){
                    SharedPreferenceManager.getInstance(applicationContext).clear()
                    val intent = Intent(applicationContext, LoginActivity::class.java)
                    intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                    startActivity(intent)
                }
                negativeButton (R.string.negative_text) {

                }
            }
        }

        /*
        Handling edit profile button
         */
        editprofile.setOnClickListener {
            val intent = Intent(applicationContext, EditProfileActivity::class.java)
            startActivity(intent)
        }

        //toolbar
        initToolbar()
    }

    private fun initToolbar(){
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}
