package com.example.ukmku.view.kenali_diri.soal

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.ukmku.R
import com.example.ukmku.models.GetTest
import com.example.ukmku.models.send_personality.PersonalitySendResult
import com.example.ukmku.utils.CommunicationSoalInterface
import com.example.ukmku.utils.SharedPreferenceManager
import com.example.ukmku.view.kenali_diri.adapter.SoalKenaliDiriAdapter
import com.example.ukmku.view.kenali_diri.hasil.KenaliDiriHasilActivity
import kotlinx.android.synthetic.main.activity_kenali_diri_soal.*


class KenaliDiriSoalActivity : AppCompatActivity(),
        CommunicationSoalInterface,
        IKenaliDiriSoalView{

    private lateinit var pagerAdapter: SoalKenaliDiriAdapter

    var hasil:ArrayList<String> = ArrayList<String>()
    var email:String = ""
    var _id:String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_kenali_diri_soal)

        getUsersLogedInData()
        getSoal()
    }

    private fun getUsersLogedInData(){
        val sharedRef = SharedPreferenceManager.getInstance(applicationContext)

        email = sharedRef.user.email.toString()
        _id = sharedRef.user._id.toString()
    }

    private fun updateSharedPref(personality: String){
        val sharedPreference = applicationContext.getSharedPreferences("ukmku_pref", Context.MODE_PRIVATE)
        val editor = sharedPreference.edit()

        editor.putString("personality", personality)
        editor.apply()
    }

    private fun getSoal(){
        setVisibleTrueLoading()

        KenaliDiriSoalPresenter(this).getKenaliDiriSoal()
    }

    fun check(personality: String, size: Int, position: Int){
        if (size != position){
            next.isClickable = !personality.isNullOrEmpty()
            next.setOnClickListener {
                if (position < 40){
                    progressbar.incrementProgressBy(2)
                }else{
                    progressbar.incrementProgressBy(1)
                }
                hasil.add(personality)
                viewPager.setCurrentItem(viewPager.currentItem+1)
                next.setBackgroundColor(resources.getColor(R.color.colorGrey))
                next.isClickable = false
                Log.d("Response", ""+hasil)
            }
        }else{
            next.text = "Selesai"
            next.setOnClickListener {
                Log.d("Result", ""+hasil)
                sendResult()
            }
        }
    }

    private fun sendResult() {
        setVisibleTrueLoading()
        KenaliDiriSoalPresenter(this).sendKenaliDiriSoal(email, hasil)
    }

    private fun setVisibleFalseLoading() {
        spin_kit.visibility = View.GONE
        viewPager.visibility = View.VISIBLE
        next.visibility = View.VISIBLE
        progressbar.visibility = View.VISIBLE
    }

    private fun setVisibleTrueLoading(){
        spin_kit.visibility = View.VISIBLE
        viewPager.visibility = View.GONE
        next.visibility = View.GONE
        progressbar.visibility = View.GONE
    }

    override fun onDataGetSoalComplete(responseSoal: List<GetTest>) {
        setVisibleFalseLoading()

        pagerAdapter = SoalKenaliDiriAdapter(supportFragmentManager, responseSoal)
        viewPager.adapter = pagerAdapter
    }

    override fun onDataGetSoalError(throwable: Throwable) {
        Toast.makeText(applicationContext, "Error "+throwable.localizedMessage, Toast.LENGTH_SHORT).show()
    }

    override fun onDataSendSoalComplete(responseComplete: PersonalitySendResult) {
        setVisibleFalseLoading()

        updateSharedPref(responseComplete.personality.name.short)

        val i = Intent(applicationContext, KenaliDiriHasilActivity::class.java)
        startActivity(i)
    }

    override fun onDataSendSoalError(throwable: Throwable) {
        Toast.makeText(applicationContext, "Error "+throwable.localizedMessage, Toast.LENGTH_SHORT).show()
    }

    override fun jawabanSatu(personality: String, size: Int, position: Int) {
        next.setBackgroundColor(resources.getColor(R.color.colorPrimary))
        check(personality, size, position)
    }

    override fun jawabanDua(personality: String, size: Int, position: Int) {
        next.setBackgroundColor(resources.getColor(R.color.colorPrimary))
        check(personality, size, position)
    }
}
