package com.example.ukmku.view.kenali_diri.hasil

import com.example.ukmku.models.send_personality.PersonalitySendResult

interface IKenaliDiriHasilView {
    fun onDataGetKepribadianComplete(body: PersonalitySendResult)
    fun onDataGetKepribadianError(throwable: Throwable)
}