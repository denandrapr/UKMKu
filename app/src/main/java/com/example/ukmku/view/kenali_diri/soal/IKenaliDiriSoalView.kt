package com.example.ukmku.view.kenali_diri.soal

import com.example.ukmku.models.GetTest
import com.example.ukmku.models.send_personality.PersonalitySendResult

interface IKenaliDiriSoalView {
    fun onDataGetSoalComplete(responseSoal: List<GetTest>)
    fun onDataGetSoalError(throwable: Throwable)

    fun onDataSendSoalComplete(responseComplete : PersonalitySendResult)
    fun onDataSendSoalError(throwable: Throwable)
}