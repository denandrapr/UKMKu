package com.example.ukmku.view.profile_edit

import com.example.ukmku.models.users.UpdateUserResponse

interface IProfileEditView {
    fun onDataUpdateComplete(responseLogin : UpdateUserResponse)
    fun onDataUpdateUnSuccess(throwable: Throwable)
}