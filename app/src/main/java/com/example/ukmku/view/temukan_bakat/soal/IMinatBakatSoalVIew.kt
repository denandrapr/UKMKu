package com.example.ukmku.view.temukan_bakat.soal

import com.example.ukmku.models.minat_bakat.GetMinatBakatResults
import com.example.ukmku.models.minat_bakat.SoalMinatBakat

interface IMinatBakatSoalVIew {
    fun onDataGetSoalComplete(responseSoal: List<SoalMinatBakat>)
    fun onDataGetSoalError(throwable: Throwable)

    fun onDataSendSoalComplete(responseComplete : GetMinatBakatResults)
    fun onDataSendSoalError(throwable: Throwable)
}