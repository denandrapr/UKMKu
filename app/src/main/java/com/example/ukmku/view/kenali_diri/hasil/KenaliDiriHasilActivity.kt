package com.example.ukmku.view.kenali_diri.hasil

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.example.ukmku.utils.SharedPreferenceManager
import kotlinx.android.synthetic.main.activity_kenali_diri_hasil.*
import com.example.ukmku.R
import com.example.ukmku.models.send_personality.PersonalitySendResult
import com.example.ukmku.view.MainActivity
import com.example.ukmku.view.kenali_diri.adapter.DescPersonalityAdapter


class KenaliDiriHasilActivity : AppCompatActivity(), IKenaliDiriHasilView {

    lateinit var personality: String
    lateinit var personality_alias: String
    lateinit var personality_short: String
    lateinit var personality_long: String
    lateinit var personality_profile: String
    lateinit var personality_karakter: String
    lateinit var personality_saran: String
    lateinit var personality_profesi: String
    lateinit var personality_partner: String
    lateinit var personality_public: String

    private lateinit var linearLayoutCharacter: LinearLayoutManager
    private lateinit var linearLayoutDevelopment: LinearLayoutManager
    private lateinit var linearLayoutPublicFigure: LinearLayoutManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_kenali_diri_hasil)

        getUsersLogedInData()

        selesai.setOnClickListener {
            val i = Intent(this, MainActivity::class.java)
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            startActivity(i)
        }
    }

    private fun getUsersLogedInData(){
        val sharedRef = SharedPreferenceManager.getInstance(applicationContext)

        personality = sharedRef.user.personality.toString()
        showLoading()
        KenaliDiriHasilPresenter(this).getHasil(personality)
    }

    override fun onDataGetKepribadianComplete(body: PersonalitySendResult) {

        linearLayoutCharacter = LinearLayoutManager(this)
        linearLayoutDevelopment = LinearLayoutManager(this)
        linearLayoutPublicFigure = LinearLayoutManager(this)

        recyclerCharacter.layoutManager = linearLayoutCharacter
        recyclerCharacter.adapter = DescPersonalityAdapter(body.personality.desc.characteristic, this)

        recyclerPengembang.layoutManager = linearLayoutDevelopment
        recyclerPengembang.adapter = DescPersonalityAdapter(body.personality.desc.developmentAdvice, this)

        recyclerPublicFigure.layoutManager = linearLayoutPublicFigure
        recyclerPublicFigure.adapter = DescPersonalityAdapter(body.personality.desc.publicFigure, this)

        personality_alias = body.personality.name.alias
        personality_short = body.personality.name.short
        personality_long = body.personality.name.long
        personality_profile = body.personality.desc.profil
        personality_profesi = body.personality.desc.profession
        personality_partner = body.personality.desc.partner

        txtShort.text = personality_short
        txtAlias.text = personality_alias
        txtProfile.text = personality_profile
        txtLong.text = personality_long
        txtProfesi.text = personality_profesi
        txtPartner.text = personality_partner

        setPersonalityImage(personality_short)
        hideLoading()
    }

    fun showLoading(){
        spin_kit.visibility = View.VISIBLE
        relative1.visibility = View.GONE
    }

    fun hideLoading(){
        spin_kit.visibility = View.GONE
        relative1.visibility = View.VISIBLE
    }

    private fun setPersonalityImage(personalitySort: String) {
        if (personalitySort.equals("entj", ignoreCase = true)){
            personalityIllustration.setBackgroundResource(R.drawable.entj)
        }else if(personalitySort.equals("esfp", ignoreCase = true)){
            personalityIllustration.setBackgroundResource(R.drawable.esfp)
        }else if(personalitySort.equals("esfp", ignoreCase = true)){
            personalityIllustration.setBackgroundResource(R.drawable.esfp)
        }else if(personalitySort.equals("entp", ignoreCase = true)){
            personalityIllustration.setBackgroundResource(R.drawable.entp)
        }else if(personalitySort.equals("istp", ignoreCase = true)){
            personalityIllustration.setBackgroundResource(R.drawable.istp)
        }else if(personalitySort.equals("istj", ignoreCase = true)){
            personalityIllustration.setBackgroundResource(R.drawable.istj)
        }else if(personalitySort.equals("isfp", ignoreCase = true)){
            personalityIllustration.setBackgroundResource(R.drawable.isfp)
        }else if(personalitySort.equals("isfj", ignoreCase = true)){
            personalityIllustration.setBackgroundResource(R.drawable.isfj)
        }else if(personalitySort.equals("intp", ignoreCase = true)){
            personalityIllustration.setBackgroundResource(R.drawable.intp)
        }else if(personalitySort.equals("intj", ignoreCase = true)){
            personalityIllustration.setBackgroundResource(R.drawable.intj)
        }else if(personalitySort.equals("estp", ignoreCase = true)){
            personalityIllustration.setBackgroundResource(R.drawable.estp)
        }else if(personalitySort.equals("estj", ignoreCase = true)){
            personalityIllustration.setBackgroundResource(R.drawable.estj)
        }
    }

    override fun onDataGetKepribadianError(throwable: Throwable) {
        
    }

    fun getImage(imageName: String?): Int {
        return resources.getIdentifier(imageName, "drawable", packageName)
    }
}
