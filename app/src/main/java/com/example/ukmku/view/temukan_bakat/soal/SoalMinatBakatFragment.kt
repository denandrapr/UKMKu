package com.example.ukmku.view.temukan_bakat.soal

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.example.ukmku.R
import com.example.ukmku.models.minat_bakat.SoalMinatBakat
import com.example.ukmku.utils.CommunicationMinatInterface
import kotlinx.android.synthetic.main.fragment_soal_minat_bakat.view.*

class SoalMinatBakatFragment : Fragment() {

    var statement:String = ""
    var modul: String = ""
    var soalSize:Int = 0
    var position:Int = 0

    private lateinit var callback:CommunicationMinatInterface

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        val root = inflater.inflate(R.layout.fragment_soal_minat_bakat, container, false)

        val args = arguments
        root.txtPernyataan.text = args!!.getString("statement")

        soalSize = args!!.getInt("size")
        position = args!!.getInt("position")
        modul = args!!.getString("modul").toString()

        callback = (activity as CommunicationMinatInterface?)!!

        root.jawaban1.setOnClickListener {
            statement = args!!.getString("statement").toString()
            if (root.jawaban1.isChecked){
                callback.jawabanSatu("null", soalSize, position, "null")
            }else{
                callback.jawabanDua("null", soalSize, position, "null")
            }
        }

        root.jawaban2.setOnClickListener{
            statement = args!!.getString("statement").toString()
            if (root.jawaban1.isChecked){
                callback.jawabanSatu(statement, soalSize, position, modul)
            }else{
                callback.jawabanDua(statement, soalSize, position, modul)
            }
        }

        return root
    }

    companion object {

        // Method for creating new instances of the fragment
        fun newInstance(soal: SoalMinatBakat, size: Int, position: Int): SoalMinatBakatFragment {

            // Store the movie data in a Bundle object
            val args = Bundle()
            args.putString("statement", soal.statement)
            args.putInt("size", size)
            args.putInt("position", (position+1))
            args.putString("modul", soal.modul)

            // Create a new MovieFragment and set the Bundle as the arguments
            // to be retrieved and displayed when the view is created
            val fragment = SoalMinatBakatFragment()
            fragment.arguments = args
            return fragment
        }
    }
}
