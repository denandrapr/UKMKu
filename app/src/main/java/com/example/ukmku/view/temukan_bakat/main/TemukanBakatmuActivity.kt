package com.example.ukmku.view.temukan_bakat.main

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.ukmku.R
import kotlinx.android.synthetic.main.activity_temukan_bakatmu.*

class TemukanBakatmuActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_temukan_bakatmu)

        mulai.setOnClickListener {
            val bottomSheetFragment = BottomSheetFragmentTemukan()
            bottomSheetFragment.show(supportFragmentManager, bottomSheetFragment.getTag())
        }
    }
}
