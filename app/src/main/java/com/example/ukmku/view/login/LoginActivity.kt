package com.example.ukmku.view.login

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.ukmku.R
import com.example.ukmku.models.users.UserResponse
import com.example.ukmku.utils.SharedPreferenceManager
import com.example.ukmku.view.MainActivity
import com.example.ukmku.view.register.RegisterMemberActivity
import kotlinx.android.synthetic.main.activity_login.*


class LoginActivity : AppCompatActivity(), ILoginView{

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        btn_regist.setOnClickListener {
            val i = Intent(this, RegisterMemberActivity::class.java)
            startActivity(i)
        }

        btn_masuk.setOnClickListener{
            loginProcess()
        }
    }

    private fun loginProcess() {
        val email:String = editTextEmail.text.toString()
        val password:String = textPassword.text.toString()

        if (email.isEmpty() && password.isNullOrEmpty()){
            Toast.makeText(this, "Tidak boleh kosong",  Toast.LENGTH_SHORT).show()
        }else{
            showSpin()
            LoginPresenter(this).sendLoginData(email, password)
        }
    }

    override fun onStart() {

        super.onStart()

        if(SharedPreferenceManager.getInstance(this).isLoggedIn){
            val intent = Intent(applicationContext, MainActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(intent)
        }
    }

    override fun onDataLoginComplete(responseLogin: UserResponse) {
        Toast.makeText(applicationContext, "Sukses login", Toast.LENGTH_SHORT).show()

        SharedPreferenceManager.getInstance(applicationContext).saveUser(responseLogin)

        val intent = Intent(applicationContext, MainActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(intent)
    }

    override fun onDataLoginUnSuccess() {
        hideSpin()
        Toast.makeText(applicationContext, "Error 101 ", Toast.LENGTH_SHORT).show()
    }

    override fun onDataLoginErrorFromApi(throwable: Throwable) {
        Toast.makeText(applicationContext, "Error 202 "+throwable.localizedMessage, Toast.LENGTH_SHORT).show()
    }

    private fun hideSpin(){
        scrollView.visibility = View.VISIBLE
        spin_kit.visibility = View.GONE
    }

    private fun showSpin(){
        scrollView.visibility = View.GONE
        spin_kit.visibility = View.VISIBLE
    }

}