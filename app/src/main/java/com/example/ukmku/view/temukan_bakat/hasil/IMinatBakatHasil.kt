package com.example.ukmku.view.temukan_bakat.hasil

import com.example.ukmku.models.minat_bakat.GetMinatBakatResults
import com.example.ukmku.models.send_personality.PersonalitySendResult

interface IMinatBakatHasil {
    fun onDataGetDataMinatComplete(body: GetMinatBakatResults)
    fun onDataGetKepribadianError(throwable: Throwable)
}