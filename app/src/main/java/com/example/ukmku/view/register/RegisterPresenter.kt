package com.example.ukmku.view.register

import android.content.Context
import android.util.Log
import com.example.ukmku.api.DataServices
import com.example.ukmku.api.ServiceBuilder
import com.example.ukmku.models.users.UserResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class RegisterPresenter (context : Context) {

    val loginView = context as IRegisterView

    fun sendRegisterData(name : String, email : String, password : String){
        val dataServices = ServiceBuilder.buildService(DataServices::class.java)
        val requestCall = dataServices.usersRegister(name, password, email)

        requestCall.enqueue(object : Callback<UserResponse> {
            override fun onResponse(call: Call<UserResponse>, response: Response<UserResponse>) {
                val body = response.body()!!
                loginView.onDataRegisterComplete(body)
            }

            override fun onFailure(call: Call<UserResponse>, t: Throwable) {
                loginView.onDataLoginErrorFromApi(t)
            }
        })
    }
}