package com.example.ukmku.view.kenali_diri.main

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.ukmku.R
import com.example.ukmku.view.kenali_diri.hasil.KenaliDiriHasilActivity
import com.example.ukmku.view.kenali_diri.soal.KenaliDiriSoalActivity
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlinx.android.synthetic.main.bottom_sheet.view.*

class BottomSheetFragment : BottomSheetDialogFragment() {

    var a:Boolean = false
    var b:Boolean = false
    var c:Boolean = false
    var d:Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val root = inflater.inflate(R.layout.bottom_sheet, container, false)

        root.checkBox.setOnCheckedChangeListener{buttonView, isChecked ->
            if (isChecked) {
                a = true
            }else{
                a = false
            }
            iff(root)
        }
        root.checkBox2.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked){
                b = true
            }else{
                b = false
            }
            iff(root)
        }
        root.checkBox3.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked){
                c = true
            }else{
                c = false
            }
            iff(root)
        }
        root.checkBox4.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked){
                d = true
            }else{
                d = false
            }
            iff(root)
        }

        return root
    }

    private fun iff(root: View) {
        if (a && b && c && d){
            root.btnMulai.setBackgroundColor(resources.getColor(R.color.colorPrimary))
            root.btnMulai.setOnClickListener {
                if (a && b && c && d){
                    val i = Intent(root.context, KenaliDiriSoalActivity::class.java)
                    root.context?.startActivity(i)
                }
            }
        }else{
            root.btnMulai.setBackgroundColor(resources.getColor(R.color.colorGrey))
        }
    }
}