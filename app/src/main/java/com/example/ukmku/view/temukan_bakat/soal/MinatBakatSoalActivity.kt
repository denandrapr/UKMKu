package com.example.ukmku.view.temukan_bakat.soal

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import com.example.ukmku.R
import com.example.ukmku.models.minat_bakat.GetMinatBakatResults
import com.example.ukmku.models.minat_bakat.SendMinatBakatResult
import com.example.ukmku.models.minat_bakat.SoalMinatBakat
import com.example.ukmku.utils.CommunicationMinatInterface
import com.example.ukmku.utils.CommunicationSoalInterface
import com.example.ukmku.utils.SharedPreferenceManager
import com.example.ukmku.view.kenali_diri.adapter.SoalMinatBakatAdapter
import com.example.ukmku.view.temukan_bakat.hasil.MinatBakatHasilActivity
import kotlinx.android.synthetic.main.activity_minat_bakat_soal.*

class MinatBakatSoalActivity : AppCompatActivity(), IMinatBakatSoalVIew, CommunicationMinatInterface {

    private lateinit var pagerAdapter: SoalMinatBakatAdapter

    var hasil:ArrayList<String> = ArrayList<String>()
    var email:String = ""
    var _id:String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_minat_bakat_soal)

        getUsersLogedInData()
        getSoal()
    }

    private fun getUsersLogedInData(){
        val sharedRef = SharedPreferenceManager.getInstance(applicationContext)

        email = sharedRef.user.email.toString()
        _id = sharedRef.user._id.toString()
    }

    private fun updateSharedPref(personality: String){
        val sharedPreference = applicationContext.getSharedPreferences("ukmku_pref", Context.MODE_PRIVATE)
        val editor = sharedPreference.edit()

        editor.putString("interest", personality)
        editor.apply()
    }

    private fun getSoal(){
        setVisibleTrueLoading()

        MinatBakatSoalPresenter(this).getSoal()
    }

    override fun onDataGetSoalComplete(responseSoal: List<SoalMinatBakat>) {
        setVisibleFalseLoading()
        pagerAdapter = SoalMinatBakatAdapter(supportFragmentManager, responseSoal)
        viewPager.adapter = pagerAdapter
    }

    override fun onDataGetSoalError(throwable: Throwable) {
        Toast.makeText(applicationContext, "Error "+throwable.localizedMessage, Toast.LENGTH_SHORT).show()
    }

    override fun onDataSendSoalComplete(responseComplete: GetMinatBakatResults) {
        Toast.makeText(applicationContext, "Sukses tapi sukses", Toast.LENGTH_SHORT).show()
        Log.d("Response", ""+responseComplete)
        updateSharedPref(responseComplete.result.interest.toString())

        val i = Intent(applicationContext, MinatBakatHasilActivity::class.java)
        startActivity(i)
    }

    override fun onDataSendSoalError(throwable: Throwable) {
        Toast.makeText(applicationContext, "Error "+throwable.localizedMessage, Toast.LENGTH_SHORT).show()
    }

    private fun setVisibleFalseLoading() {
        spin_kit.visibility = View.GONE
        viewPager.visibility = View.VISIBLE
        next.visibility = View.VISIBLE
        progressbar.visibility = View.VISIBLE
    }

    private fun setVisibleTrueLoading(){
        spin_kit.visibility = View.VISIBLE
        viewPager.visibility = View.GONE
        next.visibility = View.GONE
        progressbar.visibility = View.GONE
    }

    override fun jawabanSatu(statement: String, size: Int, position: Int, modul: String) {
        next.setBackgroundColor(resources.getColor(R.color.colorPrimary))
        check(modul, size, position)
    }

    override fun jawabanDua(statement: String, size: Int, position: Int, modul: String) {
        next.setBackgroundColor(resources.getColor(R.color.colorPrimary))
        check(modul, size, position)
    }

    fun check(modul: String, size: Int, position: Int){
        if (size != position){
            next.isClickable = !modul.isNullOrEmpty()
            next.setOnClickListener {
                if (position < 40){
                    progressbar.incrementProgressBy(2)
                }else{
                    progressbar.incrementProgressBy(1)
                }
                if (modul.equals("null")){

                }else{
                    hasil.add(modul)
                }
                viewPager.setCurrentItem(viewPager.currentItem+1)
                next.setBackgroundColor(resources.getColor(R.color.colorGrey))
                next.isClickable = false
                Log.d("Response", ""+hasil)
            }
        }else{
            next.text = "Selesai"
            next.setOnClickListener {
                Log.d("Result", ""+hasil)
//                Toast.makeText(applicationContext, "Selesai", Toast.LENGTH_SHORT).show()
                sendResult()
            }
        }
    }

    private fun sendResult() {
        MinatBakatSoalPresenter(this).sendKenaliDiriSoal(email, hasil)
    }
}
