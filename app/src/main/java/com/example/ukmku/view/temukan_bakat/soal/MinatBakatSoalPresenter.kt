package com.example.ukmku.view.temukan_bakat.soal

import android.content.Context
import android.util.Log
import com.example.ukmku.api.DataServices
import com.example.ukmku.api.ServiceBuilder
import com.example.ukmku.models.minat_bakat.GetMinatBakatResults
import com.example.ukmku.models.minat_bakat.SendMinatBakatResult
import com.example.ukmku.models.minat_bakat.SoalMinatBakat
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MinatBakatSoalPresenter(context: Context) {

    val soalView = context as IMinatBakatSoalVIew

    fun getSoal(){
        val dataServices = ServiceBuilder.buildService(DataServices::class.java)
        val requestCall = dataServices.getMinatBakatQuestion()

        requestCall.enqueue(object : Callback<List<SoalMinatBakat>> {
            override fun onResponse(call: Call<List<SoalMinatBakat>>, response: Response<List<SoalMinatBakat>>) {
                val body = response.body()!!
                Log.d("Response", ""+body)
                soalView.onDataGetSoalComplete(body)
            }

            override fun onFailure(call: Call<List<SoalMinatBakat>>, t: Throwable) {
                Log.d("Response", ""+t)
                soalView.onDataGetSoalError(t)
            }
        })
    }

    fun sendKenaliDiriSoal(email: String, hasil: ArrayList<String>) {
        val dataServices = ServiceBuilder.buildService(DataServices::class.java)
        val requestCall = dataServices.sendMinatBakatResult(email, hasil)

        requestCall.enqueue(object : Callback<GetMinatBakatResults>{

            override fun onResponse(call: Call<GetMinatBakatResults>, response: Response<GetMinatBakatResults>) {
                val body = response.body()!!
                Log.d("Response", ""+body)
                soalView.onDataSendSoalComplete(body)
            }

            override fun onFailure(call: Call<GetMinatBakatResults>, t: Throwable) {
                soalView.onDataSendSoalError(t)
            }
        })
    }

}