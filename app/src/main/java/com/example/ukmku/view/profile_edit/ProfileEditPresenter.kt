package com.example.ukmku.view.profile_edit

import android.content.Context
import com.example.ukmku.api.DataServices
import com.example.ukmku.api.ServiceBuilder
import com.example.ukmku.models.users.UpdateUserResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ProfileEditPresenter(context: Context) {

    val loginView = context as IProfileEditView

    fun sendUpdatedData(id: String,
                        name: String,
                        email: String,
                        birth: String,
                        campus: String,
                        studyProgram: String,
                        desc: String){

        val dataServices = ServiceBuilder.buildService(DataServices::class.java)
        val requestCall = dataServices.sendUpdatedData(id, name, email, birth, campus, studyProgram, desc   )

        requestCall.enqueue(object : Callback<UpdateUserResponse> {
            override fun onResponse(call: Call<UpdateUserResponse>, response: Response<UpdateUserResponse>) {
                if (response.isSuccessful){
                    val body = response.body()!!
                    loginView.onDataUpdateComplete(body)
                }
            }

            override fun onFailure(call: Call<UpdateUserResponse>, t: Throwable) {
                loginView.onDataUpdateUnSuccess(t)
            }
        })
    }

}