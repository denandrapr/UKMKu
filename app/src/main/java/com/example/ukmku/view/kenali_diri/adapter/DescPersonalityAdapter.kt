package com.example.ukmku.view.kenali_diri.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.ukmku.R
import kotlinx.android.synthetic.main.item_characteristic_personality.view.*

class DescPersonalityAdapter(
        private val character: List<String>,
        private val context: Context)
    : RecyclerView.Adapter<DescPersonalityAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DescPersonalityAdapter.ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_characteristic_personality, parent, false))
    }

    override fun getItemCount(): Int {
        return character.size
    }

    override fun onBindViewHolder(holder: DescPersonalityAdapter.ViewHolder, position: Int) {
        holder?.textCharacter.text = character.get(position).toString()
    }

    class ViewHolder(v: View) : RecyclerView.ViewHolder(v){
        val textCharacter = v.text
    }
}