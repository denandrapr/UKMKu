package com.example.ukmku.view.login

import android.content.Context
import com.example.ukmku.api.DataServices
import com.example.ukmku.api.ServiceBuilder
import com.example.ukmku.models.users.UserResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class LoginPresenter(context : Context) {

    val loginView = context as ILoginView

    fun sendLoginData(email : String, password : String){
        val dataServices = ServiceBuilder.buildService(DataServices::class.java)
        val requestCall = dataServices.usersLogin(email, password)

        requestCall.enqueue(object : Callback<UserResponse> {
            override fun onResponse(call: Call<UserResponse>, response: Response<UserResponse>) {
                if (response.isSuccessful){
                    val body = response.body()!!
                    loginView.onDataLoginComplete(body)
                }else{
                    loginView.onDataLoginUnSuccess()
                }
            }

            override fun onFailure(call: Call<UserResponse>, t: Throwable) {
                loginView.onDataLoginErrorFromApi(t)
            }
        })
    }
}