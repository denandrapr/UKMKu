package com.example.ukmku.view.temukan_bakat.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.ukmku.R
import com.example.ukmku.models.minat_bakat.GetMinatBakatResults
import kotlinx.android.synthetic.main.item_characteristic_personality.view.*
import kotlinx.android.synthetic.main.item_minat_tertinggi.view.*

class TopInterestAdapter(
        private val character: GetMinatBakatResults,
        private val context: Context)
    : RecyclerView.Adapter<TopInterestAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TopInterestAdapter.ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_minat_tertinggi, parent, false))
    }

    override fun getItemCount(): Int {
        return 3
    }

    override fun onBindViewHolder(holder: TopInterestAdapter.ViewHolder, position: Int) {
        val interest:String = character.result.interest.get(position)

        holder?.textCharacter.text = character.result.interest.get(position)
        if (interest.equals("Realistis", ignoreCase = true)){
            holder?.imgInterest.setBackgroundResource(R.drawable.ic_realistis_minat)
        }else if (interest.equals("Investigatif", ignoreCase = true)){
            holder?.imgInterest.setBackgroundResource(R.drawable.ic_investigasi_minat)
        }else if (interest.equals("Artistik", ignoreCase = true)){
            holder?.imgInterest.setBackgroundResource(R.drawable.ic_artistik_minat)
        }else if (interest.equals("Sosial", ignoreCase = true)){
            holder?.imgInterest.setBackgroundResource(R.drawable.ic_sosial_minat)
        }else if (interest.equals("Enterprising", ignoreCase = true)){
            holder?.imgInterest.setBackgroundResource(R.drawable.ic_enterprising_minat)
        }else if (interest.equals("Konvensional", ignoreCase = true)){
            holder?.imgInterest.setBackgroundResource(R.drawable.ic_konvensional_minat)
        }
    }

    class ViewHolder(v: View) : RecyclerView.ViewHolder(v){
        val textCharacter = v.textCharacter
        val imgInterest = v.icon_interest
    }
}