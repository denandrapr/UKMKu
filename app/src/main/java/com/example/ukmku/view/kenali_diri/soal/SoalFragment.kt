package com.example.ukmku.view.kenali_diri.soal

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.ukmku.R
import com.example.ukmku.models.GetTest
import com.example.ukmku.utils.CommunicationSoalInterface
import kotlinx.android.synthetic.main.item_soal_kenali_diri.view.*


class SoalFragment : Fragment(){

    var personality:String = ""
    var soalSize:Int = 0
    var position:Int = 0
    private lateinit var callback:CommunicationSoalInterface

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.item_soal_kenali_diri, container, false)
        val args = arguments
        view.jawaban1.text = args!!.getString("soal1")
        view.jawaban2.text = args!!.getString("soal2")

        soalSize = args!!.getInt("size")
        position = args!!.getInt("position")
        callback = (activity as CommunicationSoalInterface?)!!

        view.jawaban1.setOnClickListener {
            personality = args!!.getString("personality1").toString()
            if (view.jawaban1.isChecked){
                callback.jawabanSatu(personality, soalSize, position)
            }else{
                callback.jawabanDua(personality, soalSize, position)
            }
        }

        view.jawaban2.setOnClickListener{
            personality = args!!.getString("personality2").toString()
            if (view.jawaban1.isChecked){
                callback.jawabanSatu(personality, soalSize, position)
            }else{
                callback.jawabanDua(personality, soalSize, position)
            }
        }

        return view
    }

    companion object {

        // Method for creating new instances of the fragment
        fun newInstance(soal: GetTest, size: Int, position: Int): SoalFragment {

            // Store the movie data in a Bundle object
            val args = Bundle()
            args.putString("soal1", soal.statement.first.desc)
            args.putString("soal2", soal.statement.second.desc)
            args.putString("personality1", soal.statement.first.personality)
            args.putString("personality2", soal.statement.second.personality)
            args.putInt("size", size)
            args.putInt("position", (position+1))

            // Create a new MovieFragment and set the Bundle as the arguments
            // to be retrieved and displayed when the view is created
            val fragment = SoalFragment()
            fragment.arguments = args
            return fragment
        }
    }
}