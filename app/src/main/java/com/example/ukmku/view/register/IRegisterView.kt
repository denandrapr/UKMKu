package com.example.ukmku.view.register

import com.example.ukmku.models.users.UserResponse

interface IRegisterView{
    fun onDataRegisterComplete(responseRegist : UserResponse)
    fun onDataLoginErrorFromApi(throwable : Throwable)
}