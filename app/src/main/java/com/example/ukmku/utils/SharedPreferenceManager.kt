package com.example.ukmku.utils

import android.content.Context
import com.example.ukmku.models.users.UserResponse
import com.example.ukmku.models.users.User
import com.example.ukmku.models.users.UserX
import com.google.gson.Gson

class SharedPreferenceManager private constructor(private val ctx: Context){

    val isLoggedIn: Boolean
        get() {
            val sharedPreferences = ctx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE)
            return sharedPreferences.getInt("__v", -1) != -1
        }

    val user: UserX
        get() {
            val sharedPreferences = ctx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE)
            return UserX(
                    sharedPreferences.getInt("__v", -1),
                    sharedPreferences.getString("id", null),
                    sharedPreferences.getString("createdAt", null),
                    sharedPreferences.getString("email", null),
                    sharedPreferences.getString("nama", null),
                    sharedPreferences.getString("password", null),
                    sharedPreferences.getString("updatedAt", null),
                    sharedPreferences.getString("personality", null),
                    sharedPreferences.getString("description", null),
                    sharedPreferences.getString("study_program", null),
                    sharedPreferences.getString("campus", null),
                    sharedPreferences.getString("birth", null)
            )
        }

    fun clear() {
        val sharedPreferences = ctx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE)
        val editor = sharedPreferences.edit()
        editor.clear()
        editor.apply()
    }

    fun saveUser(user: UserResponse){
        val sharedPreference = ctx.getSharedPreferences(SHARED_PREF_NAME,Context.MODE_PRIVATE)
        val editor = sharedPreference.edit()

        editor.putString("email", user.user.email)
        editor.putString("nama", user.user.name)
        editor.putString("id", user.user._id)
        editor.putString("password", user.user.password)
        editor.putString("createdAt", user.user.createdAt)
        editor.putString("updatedAt", user.user.updatedAt)
        editor.putInt("__v", user.user.__v)
        if (user.user.personality.isNullOrBlank()){
            editor.putString("personality", "none")
        }else{
            editor.putString("personality", user.user.personality)
        }

        var gson = Gson()

        if (user.user.interest.isNullOrEmpty()){
            editor.putString("interest", "none")
        }else{
            var json:String = gson.toJson(user.user.interest)
            editor.putString("interest", json)
        }

        if (user.user.birth.isNullOrEmpty()){
            editor.putString("birth", "none")
        }else{
            editor.putString("birth", user.user.birth)
        }

        if (user.user.campus.isNullOrEmpty()){
            editor.putString("campus", "none")
        }else{
            editor.putString("campus", user.user.campus)
        }

        if (user.user.study_program.isNullOrEmpty()){
            editor.putString("study_program", "none")
        }else{
            editor.putString("study_program", user.user.study_program)
        }

        if (user.user.description.isNullOrEmpty()){
            editor.putString("description", "none")
        }else{
            editor.putString("description", user.user.description)
        }

        editor.apply()
    }

    companion object {
        private val SHARED_PREF_NAME = "ukmku_pref"
        private var mInstance: SharedPreferenceManager? = null
        @Synchronized
        fun getInstance(mCtx: Context): SharedPreferenceManager {
            if (mInstance == null) {
                mInstance = SharedPreferenceManager(mCtx)
            }
            return mInstance as SharedPreferenceManager
        }
    }
}