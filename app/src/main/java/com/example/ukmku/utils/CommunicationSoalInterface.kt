package com.example.ukmku.utils

interface CommunicationSoalInterface {
    fun jawabanSatu(personality: String, size: Int, position: Int)
    fun jawabanDua(personality: String, size: Int, position: Int)
}