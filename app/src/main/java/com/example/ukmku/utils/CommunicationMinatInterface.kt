package com.example.ukmku.utils

interface CommunicationMinatInterface {
    fun jawabanSatu(statement: String, size: Int, position: Int, modul: String)
    fun jawabanDua(statement: String, size: Int, position: Int, modul: String)
}