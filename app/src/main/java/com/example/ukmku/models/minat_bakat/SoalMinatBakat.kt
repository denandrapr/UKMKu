package com.example.ukmku.models.minat_bakat

data class SoalMinatBakat(
    val _id: String,
    val modul: String,
    val modul_id: Int,
    val statement: String
)