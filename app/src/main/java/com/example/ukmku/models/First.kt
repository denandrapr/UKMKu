package com.example.ukmku.models

data class First(
    val desc: String,
    val personality: String
)