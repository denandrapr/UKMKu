package com.example.ukmku.models

data class GetQuestion(
    val __v: Int,
    val _id: String,
    val answers: Answers,
    val createdAt: String,
    val modul: String,
    val modul_id: Int,
    val question: String,
    val updatedAt: String
)