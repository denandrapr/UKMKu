package com.example.ukmku.models.send_personality

data class Desc(
    val characteristic: List<String>,
    val developmentAdvice: List<String>,
    val partner: String,
    val profession: String,
    val profil: String,
    val publicFigure: List<String>
)