package com.example.ukmku.models

data class Statement(
    val first: First,
    val second: Second
)