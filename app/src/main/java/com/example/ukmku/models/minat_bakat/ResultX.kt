package com.example.ukmku.models.minat_bakat

data class ResultX(
    val count: Int,
    val value: String
)