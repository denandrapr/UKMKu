package com.example.ukmku.models.send_personality

data class Personality(
    val _id: String,
    val desc: Desc,
    val name: Name
)