package com.example.ukmku.models.minat_bakat

data class SendMinatBakatResult(
    val email: String,
    val results: List<String>
)