package com.example.ukmku.models.minat_bakat

data class Result(
    val __v: Int,
    val _id: String,
    val createdAt: String,
    val email: String,
    val interest: List<String>,
    val results: List<ResultX>,
    val updatedAt: String
)