package com.example.ukmku.models

data class Second(
    val desc: String,
    val personality: String
)