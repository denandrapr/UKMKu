package com.example.ukmku.models.users

import com.example.ukmku.models.users.User

data class UserResponse(
    val user: User
)