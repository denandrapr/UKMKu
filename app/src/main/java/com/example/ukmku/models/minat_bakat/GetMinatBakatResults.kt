package com.example.ukmku.models.minat_bakat

data class GetMinatBakatResults(
    val result: Result
)