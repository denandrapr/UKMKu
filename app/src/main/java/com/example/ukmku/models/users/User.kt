package com.example.ukmku.models.users

data class User(
    val __v: Int,
    val _id: String?,
    val createdAt: String?,
    val email: String?,
    val name: String?,
    val password: String?,
    val updatedAt: String?,
    val personality: String?,
    val interest: List<Any>,
    val description: String?,
    val study_program: String?,
    val campus: String?,
    val birth: String?
)