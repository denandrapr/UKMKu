package com.example.ukmku.models.send_personality

data class Name(
    val alias: String,
    val long: String,
    val short: String
)