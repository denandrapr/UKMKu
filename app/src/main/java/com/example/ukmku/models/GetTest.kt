package com.example.ukmku.models

data class GetTest(
    val __v: Int,
    val _id: String,
    val createdAt: String,
    val modul: String,
    val modul_id: Int,
    val statement: Statement,
    val updatedAt: String
)