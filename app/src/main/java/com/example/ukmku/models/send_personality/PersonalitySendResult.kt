package com.example.ukmku.models.send_personality

data class PersonalitySendResult(
    val personality: Personality
)